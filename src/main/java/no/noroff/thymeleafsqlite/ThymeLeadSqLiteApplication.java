package no.noroff.thymeleafsqlite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeLeadSqLiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThymeLeadSqLiteApplication.class, args);
    }

}
