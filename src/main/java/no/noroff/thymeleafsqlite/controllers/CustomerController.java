package no.noroff.thymeleafsqlite.controllers;

import no.noroff.thymeleafsqlite.data_access.CustomerRepository;
import no.noroff.thymeleafsqlite.models.Customer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;

@Controller
public class CustomerController {

    CustomerRepository crep = new CustomerRepository();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(){
        return "index";
    }

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public String getAllCustomers(Model model){
        model.addAttribute("customers", crep.getAllCustomers());
        return "view-customers";
    }

    @RequestMapping(value = "/customers/{id}")
    public String getSpecificCustomer(@PathVariable String id, Model model){
        model.addAttribute("customer", crep.getSpecificCustomer(id));
        return "view-customer";
    }

    @RequestMapping(value = "/add-customers", method = RequestMethod.GET)
    public String addCustomer(Model model){
        model.addAttribute("customer", new Customer());
        return "add-customers";
    }

    @RequestMapping(value = "/add-customers", method = RequestMethod.POST)
    public String addCustomer(@ModelAttribute Customer customer, BindingResult error, Model model){
        Boolean success = crep.addCustomer(customer);
        model.addAttribute("success", success);
        if(success){model.addAttribute("customer", new Customer());}
        return "add-customers";
    }

}
