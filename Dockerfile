FROM openjdk:14
ADD target/thymeleafsqlite-1.jar tlsqlite.jar
ENTRYPOINT [ "java", "-jar", "/tlsqlite.jar" ]